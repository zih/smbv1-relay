# Einrichtung eines 

In diesem Dokument wird beschrieben, wie Sie einen SMBv1 Passthrough Share einrichten können. Obwohl SMBv1 aufgrund von Sicherheitsbedenken nicht mehr unterstützt wird, gibt es Szenarien, in denen eine Rückwärtskompatibilität erforderlich ist. Für diese Fälle bietet diese Anleitung eine Lösung.

## Ressourcen
- [Gruppenlaufwerk mit NFS-Berechtigung](https://selfservice.tu-dresden.de/services/groupdrive/)
- Virtuelle Maschine - [Research-Cloud](https://selfservice.tu-dresden.de/services/research-cloud/) oder [Enterprise-Cloud](https://selfservice.tu-dresden.de/services/enterprise-cloud/)
- [Client-Computer mit Ansible (Playbook)](https://gitlab.hrz.tu-chemnitz.de/zih/smbv1-relay)

# Gruppenlaufwerk mit NFS Berechtigung

Gehen Sie auf das [Self-Service-Portal](https://selfservice.tu-dresden.de/services/groupdrive/) und wählen Sie den Reiter "Gruppenlaufwerk Übersicht". Klicken Sie dort auf "Gruppenlaufwerk beantragen". Geben Sie die notwendigen Informationen sowie die gewünschte Größe des Laufwerks an. Unter "Konfiguration" wählen Sie aus dem Drop-down-Menü die Option "Erweiterte Konfiguration" aus. Klicken Sie auf "Weiter", wählen Sie als Berechtigungsmodell NTFS aus und aktivieren Sie im Checkbox-Menü NFS. Im nächsten Schritt geben Sie die IP-Adresse Ihrer bestellten VM bei den NFS-Freigaben an und senden den Antrag ab.

Wenn Sie bereits ein Gruppenlaufwerk mit NFS haben, können Sie in den Einstellungen des Laufwerks auf "NFS-Freigaben editieren" gehen und dort die IP-Adresse Ihrer VM hinzufügen. Sollte dies nicht möglich sein, wenden Sie sich bitte an den Service-Desk und lassen Sie Ihr Gruppenlaufwerk entsprechend anpassen.

# Virtuelle Maschine

Um einen Samba-Server zu installieren, benötigen Sie eine virtuelle Maschine (VM). An der TU-Dresden haben Sie die Möglichkeit, eine VM in der Research-Cloud oder in der Enterprise-Cloud zu beantragen.

## Research-Cloud

Besuchen Sie das [Self-Service-Portal](https://selfservice.tu-dresden.de/services/research-cloud/) und wählen Sie den Reiter "VM Management". Klicken Sie auf "Neue VM erstellen". Geben Sie Ihre Organisationseinheit an und vergeben Sie einen eindeutigen Namen für die VM (Empfehlung: Samba-Server). Als Systemvorlage wählen Sie eine aktuelles Debian aus. Akzeptieren Sie die Nutzungsbedingungen und klicken Sie auf "Virtuelle Maschine erstellen". Nach der Erstellung steht die VM quasi sofort bereit. Jedoch muss der Bestand dieser jährlich über das Self-Service-Portal verlängert werden.

## Enterprise-Cloud

Die Beantragung einer VM in der [Enterprise-Cloud](https://selfservice.tu-dresden.de/services/enterprise-cloud/) ist aufwendiger, da die Freischaltung manuell erfolgt. Die VM steht also nicht sofort bereit. Dafür ist diese VM persistent und wird automatisch gepflegt und gewartet. Für SMBv1 Passthrough müssen Sie den Port für das SMB-Protokoll nach außen öffnen (Port 445).

## Vorbereitung der VM

Nachdem Sie Ihre VM erhalten haben, sollten Sie ein passwortloses sudo einrichten, um den Benutzer `service` zum Superuser zu machen. `service` ist ein Standardbenutzer der VM. Gehen Sie folgendermaßen vor:

- Verbinden Sie sich per SSH mit Ihrer VM: `ssh service@IPAdresseDerVM`. Das Passwort für die SSH-Verbindung finden Sie im Self-Service-Portal unter den Einstellungen Ihrer VM.
- Öffnen Sie die sudoers-Datei mit einem Texteditor, indem Sie folgenden Befehl eingeben: `sudo visudo`.
- Fügen Sie am Ende der Datei die folgende Zeile hinzu: `service ALL=(ALL) NOPASSWD: ALL`.
- Speichern Sie die Änderungen und schließen Sie den Texteditor.

Optional: Um in Zukunft ohne Passworteingabe per SSH auf Ihre VM zugreifen zu können, können Sie Ihren SSH-Schlüssel auf der VM hinterlegen.

# Client-Computer mit Ansible

## Ansible installieren

### Für Linux-Nutzer:
Ansible lässt sich meistens problemlos aus den offiziellen Paketquellen der jeweiligen Distribution installieren.
Für auf Debian basierende Systeme lautet der Installationsbefehl: `sudo apt install ansible`. Beachten Sie, dass der Paketname leicht variieren kann, je nachdem, welche Distribution Sie verwenden.

Alternativ bzw. wenn Sie eine aktuelle Version von Ansible benötigen, kann die Installation auch mit Pip, einem Paketverwaltungssystem für Python, erfolgen: `python3 -m pip install --user ansible`.

Überprüfen Sie die Installation anschließend mit dem Befehl `ansible --version`.

### Für Windows-Nutzer:
Für Windows-Nutzer gibt es verschiedene Methoden, das Playbook zu nutzen. Die einfachste Methode besteht darin, Ansible in der zuvor erstellten VM zu installieren. Eine weitere, relativ einfache Möglichkeit bietet das Windows Subsystem for Linux (WSL2), das Ihnen erlaubt, eine Linux-Umgebung auf Ihrem Windows-System zu nutzen.

Sofern Sie sich dazu entscheiden, Ansible direkt in der VM zu installieren und das Plabook dort ausführen, ist der `ansible_host` mit `localhost` anzugeben.

## Playbook clonen

Mit dem Befehl `git clone https://gitlab.hrz.tu-chemnitz.de/zih/smbv1-relay.git` klonen Sie sich die notwendige Playbook aus dem Repository. Verwenden Sie dann den Befehl `cd ~/smbv1-relay`, um in das Verzeichnis zu gelangen.

## Konfiguration

Das Playbook übernimmt die Installation von Samba und führt alle erforderlichen Konfigurationen durch, um eine Verbindung zwischen Ihrem Client und der VM herzustellen, sodass Sie auf Ihr Gruppenlaufwerk zugreifen können. Um dies zu ermöglichen, müssen Sie einige Einstellungen in der Datei `~/smbv1-relay/hosts.yml` vornehmen.

### Beispieldatei

```yml
---
all:
  vars:
    ansible_python_interpreter: /usr/bin/python3
    ansible_user: service
    ansible_become: true
  hosts:
    sambaserver:
      ansible_host: 172.26.x.x
      samba__groupdrive_path: vs-grpXX.zih.tu-dresden.de:/laufwerk
      samba__smbuser_password: sicheresPasswort
      samba__share_directory: /mnt/sambashare
```

### Pflichtvariablen

Diese Variablen müssen angepasst werden:

- `ansible_user: service`: Ist der Standarduser Ihrer VM.    
- `ansible_host: 172.26.x.x`: Ist die IP-Adresse Ihrer VM.  
- `samba__share_directory: /mnt/sambashare`: Ist der Pfad in dem das Gruppenlaufwerk gemountet wird.  
- `samba__smbuser_password: sicheresPasswort`: Das Passwort Ihres Users.
- `samba__groupdrive_path: vs-grpXX.zih.tu-dresden.de:/laufwerk`: Ist der Pfad Ihres Gruppenlaufwerks.  

### Nutzer und Gruppe konfigurieren

Das Playbook konfiguriert standardmäßig `smbuser` als Benutzer und `smbusers` als Gruppe. Passen Sie dennoch diese Variablen an Ihre Umgebung an:

```yml
      samba__smbuser_name: smbuser
      samba__smbuser_uid: 10000
      samba__group_name: smbusers
      samba__smbuser_gid: 10000
```

## Playbook Ausführen
Um das Ansible-Playbook auszuführen, wechseln Sie auf die Kommandozeile Ihres Clients und führen folgenden Befehl aus: `ansible-playbook ~/smbv1-relay/site.yml` 

Weitere nützliche Optionen sind:
- `-b, --become`: Führt Aufgaben mit Berechtigungserhöhung (sudo) aus, wenn keine passwortlose sudo-Konfiguration vorliegt.
- `-k, --ask-pass`: Fragt nach einem Passwort für die SSH-Verbindung, falls kein SSH-Schlüssel hinterlegt ist.
- `-v, --verbose`: Erzeugt ausführlichere Ausgaben zur Fehlerbehebung.

# Troubleshooting

## Verbindung testen
Die Konfiguration lässt sich einfach mit dem Programm `smbclient` testen. Geben Sie den Befehl `smbclient //172.26.x.x/sambashare -U smbuser` ein. Nach der Passworteingabe sollten Sie Zugriff auf das Gruppenlaufwerk haben.

## Keine Verbindung möglich?
Sollten Verbindungsprobleme auftreten, können folgende Schritte zur Fehlerbehebung hilfreich sein:

- Verwenden Sie die Option `-vvv` für maximale Debug-Ausgaben.
- Der Befehl `journalctl` zeigt kürzlich erfolgte Logeinträge an, die unter Umständen genauere Informationen als der Debugmodus von Ansible bieten.
- Durchsuchen Sie das Verzeichnis `/var/log`, am besten mit l`s -la /var/log/samba`.
- Überprüfen Sie, ob das Gruppenlaufwerk korrekt eingestellt ist.
- Stellen Sie sicher, dass uid und gid eindeutig sind.
- Prüfen Sie die Firewall-Freigaben (besonders bei der Enterprise-Cloud).
- Überprüfen Sie, ob die notwendigen Dienste aktiv sind (sshd, smbd).
- Schauen Sie auf dem Zielsystem nach, ob die Ordner und Dateien korrekt angelegt wurden.

Ansible Dokumentation: https://docs.ansible.com
